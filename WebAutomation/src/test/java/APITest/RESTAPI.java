package APITest;

import io.restassured.response.Response;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by bipin on 20/07/17.
 */
public class RESTAPI {

    Response response;
    String correctText;
    ArrayList<Integer> arrayList= new ArrayList<Integer>();

    @Test

    public Map<Integer, Integer> getpostbyId(String postId)
    {
        HashMap<Integer, Integer> hashMap= new HashMap<Integer, Integer>();
        int choices;
        response=given()
                .header("Cookie", "connect.sid=s%3AooN3gyYP9nUEyjS11nfUrKoQ7lpYCiPl.eOE7hONJr3L43j0zs0UiuVObus9lEyBWySebKby9GAs ; __cfduid=dbe5781ddeab7c25c71c55d2fa2b27e031505559818")
                .header("isMobile", true)
                .header("appVersion", 1)
                .header("timezone", "GMT+05:30")
                .header("accept-encoding", "gzip")
                .header("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
                .when()
                .param("postId", postId)
                .param("apiVersion", 1)
                .post("https://api.gradeup.co/posts/getPostById");
        Assert.assertEquals(response.statusCode(), 200);

        System.out.println(response.body().asString());
        JSONObject jsonObject= new JSONObject(response.body().asString());
        System.out.println(jsonObject);


        int questionData= jsonObject.getJSONObject("posttext").getJSONArray("questionData").length();
//        System.out.println("Questions count is: "+questionData);
        for (int i= 0; i< questionData; i++)
        {

            arrayList.add(jsonObject.getJSONObject("posttext").getJSONArray("questionData").getJSONObject(i).getInt("id"));

            choices= jsonObject.getJSONObject("posttext").getJSONArray("questionData").getJSONObject(i).getJSONArray("choices").length();
            for (int j= 0; j< choices; j++)
            {
                if (jsonObject.getJSONObject("posttext").getJSONArray("questionData").getJSONObject(i).getJSONArray("choices").getJSONObject(j).getBoolean("isCorrect"))
                {
                    correctText= jsonObject.getJSONObject("posttext").getJSONArray("questionData").getJSONObject(i).getJSONArray("choices").getJSONObject(j).getString("optionTxt");
//                    System.out.println(correctText);
                    hashMap.put(i, j);
                    break;
                }
            }
        }
        return hashMap;
    }


    public double getBoostLevel(String postId) {
        response=given()
                .header("Cookie", "connect.sid=s%3AooN3gyYP9nUEyjS11nfUrKoQ7lpYCiPl.eOE7hONJr3L43j0zs0UiuVObus9lEyBWySebKby9GAs ; __cfduid=dbe5781ddeab7c25c71c55d2fa2b27e031505559818")
                .header("isMobile", true)
                .header("appVersion", 1)
                .header("timezone", "GMT+05:30")
                .header("accept-encoding", "gzip")
                .header("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
                .when()
                .param("postId", postId)
                .param("apiVersion", 1)
                .post("https://api.gradeup.co/posts/getPostById");
        Assert.assertEquals(response.statusCode(), 200);
        JSONObject jsonObject= new JSONObject(response.body().asString());
        return jsonObject.getDouble("boostlevel");
    }

    @Test

    public void bookmarkQuestions()
    {
        System.out.println(arrayList);
        for (int i= 0; i< arrayList.size(); i++)
        {
            response = given()
                    .header("Cookie", "connect.sid=s%3AooN3gyYP9nUEyjS11nfUrKoQ7lpYCiPl.eOE7hONJr3L43j0zs0UiuVObus9lEyBWySebKby9GAs ; __cfduid=dbe5781ddeab7c25c71c55d2fa2b27e031505559818")
                    .header("isMobile", true)
                    .header("appVersion", 1)
                    .header("timezone", "GMT+05:30")
                    .header("accept-encoding", "gzip")
                    .header("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
                    .when()
                    .param("type", "question")
                    .param("id", arrayList.get(i))
                    .post("https://api.gradeup.co/bookMarks");
            System.out.println(response.body().asString());
        }
    }

}
