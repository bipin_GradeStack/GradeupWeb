package APITest;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by bipin on 24/07/17.
 */
public class HttpURLConnectionTest {

    public int sendGetRequest(String link) throws IOException {
        URL url= new URL(link);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setConnectTimeout(10000);
        connection.setRequestMethod("GET");
        connection.setRequestProperty("accept", "application/json, text/javascript, */*; q=0.01");
        connection.setRequestProperty("accept-encoding", "gzip, deflate, sdch, br");
        connection.setRequestProperty("accept-language", "en-US,en;q=0.8,hi;q=0.6");
        connection.setRequestProperty("cache-control", "no-cache");
        connection.setRequestProperty("user-agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36");
        connection.setRequestProperty("x-requested-with", "XMLHttpRequest");

        System.out.println("Sending get request : "+ url);
        int responseCode = connection.getResponseCode();

        System.out.println("Response code : "+ responseCode);
        return responseCode;
    }

}
