package PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created by bipin on 11/05/17.
 */
public class ExamCategory {
    WebDriver driver;

    @FindBy(xpath = "//a[@class= 'box__item']")
    List<WebElement> box;

    @FindBy(tagName = "h2")
    WebElement boxName;

    @FindBy(tagName = "img")
    WebElement boxImageLink;

    @FindBy(tagName = "href")
    WebElement boxLink;

    public ExamCategory(WebDriver driver)
    {
        this.driver= driver;
        PageFactory.initElements(driver, this);
    }

    public Integer getBoxSize()
    {
        return box.size();
    }

    public String getBoxName()
    {
        return boxName.getText();
    }

    public String getBoxImageLink()
    {
        return boxImageLink.getAttribute("src");
    }

    public String getBoxLink()
    {
        return boxLink.getAttribute("href");
    }
}
