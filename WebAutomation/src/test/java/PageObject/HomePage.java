package PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created by bipin on 11/05/17.
 */
public class HomePage
{
    WebDriver driver;

    @FindBy(xpath = "//label[@class= 'rocket-green']")
    WebElement study;

    @FindBy(xpath = "//a[@class= 'auth__login js-show-login']")
    WebElement login;

    @FindBy(xpath = "//a[@class= 'auth__register js-show-register']")
    WebElement register;

    @FindBy(xpath = "//input[@type= 'search']")
    WebElement search;

    @FindBy(xpath = "//span[@class= 'google-red-bg safed upper-case register-btn js-show-register']")
    WebElement registerNow;

    @FindBy(xpath = "//div[@class= 'login-register-container__data']//p//span")
    List<WebElement> loginAgain;


    @FindBy(xpath = "//a[@class= ' s-15 main-header__explore-panel-item']")
    List<WebElement> studyExams;

    @FindBy(xpath = "//div[@class= 'mobile_img flex-box']//img")
    WebElement gradeupImage;

    @FindBy(xpath = "//p[@class= 'download_btns flex-box flex-row']//a//img")
    WebElement googlePlayButton;

    @FindBy(xpath = "//p[@class= 'download_btns flex-box flex-row']//span//input")
    WebElement getSMS;

    @FindBy(xpath = "//div[@class= 'download_app_by_sms']//h1")
    WebElement usersCount;

    @FindBy(xpath = "//div[@class= 'download_app_by_sms']//p")
    List<WebElement> downloadText;

    @FindBy(xpath = "//section[@class= 'flex-box flex-row footer-section-links']//a")
    List<WebElement> footer;

    @FindBy(xpath = "//div[@class= 'download_app_by_sms']//form//div//input")
    WebElement mobilePlaceHolder;

    @FindBy(xpath = "//p[@class= 'footer__company-name s-14 light-grey upper-case']")
    WebElement companyName;

    @FindBy(xpath = "//section[@class= 'flex-box flex-row footer-section-social']//a")
    List<WebElement> socialAccount;

    @FindBy(xpath = "//input[@name= 'password']")
    List<WebElement> password;

    @FindBy(xpath = "//input[@name= 'email']")
    List<WebElement> email;

    @FindBy(xpath = "//input[@class= 'btn auth__button js-auth-login']")
    List<WebElement> loginButton;

    public void clickOnLoginButton()
    {
        loginButton.get(1).click();
    }

    public HomePage(WebDriver driver)
    {
        this.driver= driver;
        PageFactory.initElements(driver, this);
    }

    public void clickOnPassword()
    {
        password.get(2).click();
    }

    public WebElement passwordField()
    {
        return password.get(2);
    }

    public void clickOnLogin()
    {
        email.get(2).click();
    }

    public WebElement emailField()
    {
        return email.get(2);
    }

    public WebElement getLoginWebElement()
    {
        return login;
    }

    public String getStudyText()
    {
        return study.getText();
    }

    public void clickStudy()
    {
        study.click();
    }

    public String loginText()
    {
        return login.getText();
    }

    public void clickLogin() {  login.click();  }

    public String registerText()
    {
        return register.getText();
    }

    public String searchText()
    {
        return search.getAttribute("placeholder");
    }

    public String registerNowText()
    {
        return registerNow.getText();
    }

    public List<WebElement> getloginAgainText()
    {
        return loginAgain;
    }

    public List<WebElement> getStudyExams()
    {
        return studyExams;
    }

    public String getImageSource()
    {
        return gradeupImage.getAttribute("src");
    }

    public String getGooglePlaySource()
    {
        return googlePlayButton.getAttribute("src");
    }

    public String getGooglePlayText()
    {
        return googlePlayButton.getAttribute("alt");
    }

    public String getSMStext()
    {
        return getSMS.getAttribute("value");
    }

    public String getUsersCountText()
    {
        return usersCount.getText();
    }

    public String getDownloadText()
    {
        return downloadText.get(0).getText();
    }

    public String getLinkText()
    {
        return downloadText.get(1).getText();
    }

    public String getAboutUSLink()
    {
        return footer.get(0).getAttribute("href");
    }

    public String getAboutUSText()
    {
        return footer.get(0).getText();
    }

    public String getFAQLink()
    {
        return footer.get(1).getAttribute("href");
    }

    public String getFAQText()
    {
        return footer.get(1).getText();
    }

    public String getTermsLink()
    {
        return footer.get(2).getAttribute("href");
    }

    public String getTermsText()
    {
        return footer.get(2).getText();
    }

    public String getMobilePlaceHolderText()
    {
        return mobilePlaceHolder.getAttribute("placeholder");
    }

    public String getCompanyName()
    {
        return companyName.getText();
    }

    public String getFacebookLink()
    {
        return socialAccount.get(0).getAttribute("href");
    }

    public String getTwitterLink()
    {
        return socialAccount.get(1).getAttribute("href");
    }

    public String getFacebookTitle()
    {
        return socialAccount.get(0).getAttribute("title");
    }

    public String getTwitterTitle()
    {
        return socialAccount.get(1).getAttribute("title");
    }

}
