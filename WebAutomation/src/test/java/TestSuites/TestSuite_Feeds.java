package TestSuites;

import APITest.RESTAPI;
import PageObject.HomePage;
import org.apache.xerces.util.SynchronizedSymbolTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import sun.awt.windows.ThemeReader;

import java.util.List;

/**
 * Created by bipin on 23/05/17.
 */
public class TestSuite_Feeds extends TestBaseSetup
{
    int count= 0;
    WebDriver driver;
    RESTAPI restapi;

    @BeforeMethod
    public void setUp()
    {
        driver=getDriver();
        System.out.println("driver: "+driver);
        restapi= new RESTAPI();
    }


    @Test

    public void TC01_GetDefaultPost()
    {
        count= driver.findElements(By.xpath(".//article[contains(@class, 'post box-shadow ajax-image-upload-container')]")).size();
        System.out.println(count);
    }

    @Test

    public void TC02_ScrollDownFeeds() throws InterruptedException
    {
        for (int i= 0; i< 1; i++)
        {
            Thread.sleep(2000);
            javaScriptExecutor.executeScript("window.scrollTo(0, document.body.scrollHeight);");
        }
    }


    @Test

    public void TC03_BookmarkPost() throws InterruptedException
    {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class= 'bookmark-post']")));
        for (int i= 0; i< count; i++)
        {
            driver.findElements(By.xpath("//div[@class= 'bookmark-post']")).get(i).click();
            Thread.sleep(2000);
        }
    }

    @Test

    public void TC04_Upvote() throws InterruptedException
    {
        WebElement upvote;
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//a[@class= 'post__upvote-action flex-box flex-row js-upvote-post']")));
        for (int i= 0; i< count; i++)
        {
            upvote= driver.findElements(By.xpath(".//a[@class= 'post__upvote-action flex-box flex-row js-upvote-post']")).get(i);
            javaScriptExecutor.executeScript("arguments[0].click();", upvote);
            Thread.sleep(2000);
        }
    }

    @Test

    public void TC05_GetBoostLevel()
    {
        double boostScore;
        List<WebElement> post= driver.findElements(By.xpath(".//article[contains(@class, 'post box-shadow ajax-image-upload-container')]"));
        for (int i= 0; i< post.size(); i++)
        {
            String postId = post.get(i).getAttribute("data-id");
            boostScore= restapi.getBoostLevel(postId);
            System.out.println("Boostlevel for "+postId+" is: "+boostScore);
            if (boostScore < 0.1)
            {
                System.out.println("Bug Found!");
            }
        }
    }

    @Test

    public void TC06_Comment() throws InterruptedException
    {
        driver.findElements(By.xpath("//a[@class= 'post__comment-action js-open-comments flex-box flex-row']")).get(0).click();
        Thread.sleep(5000);
    }

}
