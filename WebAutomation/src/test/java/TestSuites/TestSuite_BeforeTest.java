package TestSuites;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

public class TestSuite_BeforeTest
{
    String baseURL= "http://52.72.175.90:3008";
    WebDriver driver;
    WebDriverWait wait;
    JavascriptExecutor jse;

    public void setup()
    {
        System.setProperty("webdriver.chrome.driver", "/home/bipin/chromedriver");
        ChromeOptions options= new ChromeOptions();
        options.addArguments("-incognito");
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        LoggingPreferences loggingprefs = new LoggingPreferences();
        loggingprefs.enable(LogType.BROWSER, Level.ALL);
        capabilities.setCapability(CapabilityType.LOGGING_PREFS, loggingprefs);
        driver = new ChromeDriver(capabilities);
        jse = (JavascriptExecutor)driver;

        driver.get("chrome://extensions-frame");
        WebElement checkbox = driver.findElement(By.xpath("//label[@class='incognito-control']/input[@type='checkbox']"));
        if (!checkbox.isSelected()) {
            checkbox.click();
        }

        wait= new WebDriverWait(driver, 100);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.get(baseURL);
        driver.manage().window().maximize();
        System.out.println("Before Test executed!");
    }
}
