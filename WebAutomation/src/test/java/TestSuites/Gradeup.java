package TestSuites;

import com.sun.xml.internal.xsom.impl.Ref;
import io.restassured.http.ContentType;
import io.restassured.internal.http.ContentEncoding;
import io.restassured.response.Response;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import static io.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

/**
 * Created by bipin on 12/05/17.
 */
public class Gradeup
{
    WebDriver driver;
    String baseURL= "https://gradeup.co";
//    JavascriptExecutor jse;
    String currentWindowHandle;
    WebDriverWait wait;
    String parentWindow;
    Set<String> set;
    Actions actions;
    JavascriptExecutor javascriptExecutor;

//    @BeforeMethod
//
//    public void beforeMethod()
//    {
//        javascriptExecutor= (JavascriptExecutor) driver;
//    }

    @BeforeTest

    public void setup()
    {
        System.setProperty("webdriver.chrome.driver", "/Users/bipin/Downloads/chromedriver");
        ChromeOptions options= new ChromeOptions();
        options.addArguments("-incognito");
        options.addArguments("start-fullscreen");

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        LoggingPreferences loggingprefs = new LoggingPreferences();
        loggingprefs.enable(LogType.BROWSER, Level.SEVERE);
        capabilities.setCapability(CapabilityType.LOGGING_PREFS, loggingprefs);
        driver = new ChromeDriver(capabilities);
        javascriptExecutor = (JavascriptExecutor)driver;

        driver.get("chrome://extensions-frame");
        WebElement checkbox = driver.findElement(By.xpath("//label[@class='incognito-control']/input[@type='checkbox']"));
        if (!checkbox.isSelected()) {
            checkbox.click();
        }

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.get(baseURL);

        currentWindowHandle = this.driver.getWindowHandle();
        driver.switchTo().window(currentWindowHandle);

        wait= new WebDriverWait(driver, 30);
        set= new HashSet<String>();
        actions= new Actions(driver);
        parentWindow= driver.getWindowHandle();
        System.out.println("Before Test executed!");
    }

    @AfterTest
    public void afterTest()
    {
        LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER);
        for (LogEntry entry : logEntries) {
            System.out.println(new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage());
        }
        driver.quit();
    }
}
