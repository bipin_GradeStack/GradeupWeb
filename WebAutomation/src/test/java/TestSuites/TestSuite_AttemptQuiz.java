package TestSuites;

import APITest.RESTAPI;
import PageObject.HomePage;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.Serializable;
import java.util.*;

/**
 * Created by bipin on 28/06/17.
 */
public class TestSuite_AttemptQuiz extends TestBaseSetup
{
    String url= "https://gradeup.co/banking-insurance?feedTab=quizzes";
    String parentWindow, postid, cookie;
    HomePage homePage;
//    Group group;
//    Actions actions;
    Set<String> childWindow;
    RESTAPI restapi;
    HashMap<Integer, Integer> hashMap;



    WebDriver driver;


    @BeforeMethod

    public void beforeMethod()
    {

//        homePage= new HomePage(driver);

        driver=getDriver();
        System.out.println("driver: "+driver);

        hashMap= new HashMap<Integer, Integer>();
        restapi= new RESTAPI();
        parentWindow= driver.getWindowHandle();
//        group= new Group(driver);
//        actions= new Actions(driver);
    }

    /*

    @Test

    public void TC01_Login() throws InterruptedException
    {
        wait.until(ExpectedConditions.elementToBeClickable(homePage.getLoginWebElement()));
        homePage.clickLogin();
        homePage.clickOnLogin();
        homePage.emailField().sendKeys("rocking.chap08@gmail.com");
        homePage.clickOnPassword();
        homePage.passwordField().sendKeys("123456");
        homePage.clickOnLoginButton();
        driver.findElements(By.xpath("//a[@class= 'checkbox checkbox--large checkbox--no-limit']")).get(0).click();
        System.out.println("Logged In successfully!");
        Thread.sleep(5000);
        System.out.println(driver.manage().getCookies());
//        System.out.println("__cfduid="+driver.manage().getCookieNamed("__cfduid"));
//        System.out.println("__gads"+driver.manage().getCookieNamed("__gads"));
//        System.out.println("device_id"+driver.manage().getCookieNamed("device_id"));
//        System.out.println("G_ENABLED_IDPS"+driver.manage().getCookieNamed("G_ENABLED_IDPS"));
//        System.out.println("_col_uuid"+driver.manage().getCookieNamed("_col_uuid"));
//        System.out.println("cookie_mweb"+driver.manage().getCookieNamed("cookie_mweb"));
//        System.out.println("connect.sid"+driver.manage().getCookieNamed("connect.sid"));
//        System.out.println("_gat"+driver.manage().getCookieNamed("_gat"));
//        System.out.println("_dc_gtm_UA-80480423-1"+driver.manage().getCookieNamed("_dc_gtm_UA-80480423-1"));
//        System.out.println("_gid"+driver.manage().getCookieNamed("_gid"));
//        System.out.println("_gat_UA-80480423-1"+driver.manage().getCookieNamed("_gat_UA-80480423-1"));
//        System.out.println("statefree"+driver.manage().getCookieNamed("statefree"));
//        System.out.println("G_AUTHUSER_H"+driver.manage().getCookieNamed("G_AUTHUSER_H"));
//        System.out.println("_gaexp"+driver.manage().getCookieNamed("_gaexp"));
        cookie= driver.manage().getCookieNamed("connect.sid").toString() + driver.manage().getCookieNamed("__cfduid").toString();
        System.out.println(cookie);
    }

    */

    @Test

    public void TC02_ScrollDown() throws InterruptedException
    {
        driver.get(url);
        for (int i= 0; i< 1; i++)
        {
            Thread.sleep(2000);
            javaScriptExecutor.executeScript("window.scrollTo(0, document.body.scrollHeight);");
        }
    }

    @Test

    public void TC03_AttemptTest() throws InterruptedException
    {
        WebElement choice;
        List<WebElement> quiz= driver.findElements(By.xpath("//article[@class= 'post box-shadow ajax-image-upload-container post--test']"));
        for (int i= 0; i< quiz.size(); i++)
        {
            javaScriptExecutor.executeScript("arguments[0].scrollIntoView(true);", quiz.get(i));
            Thread.sleep(3000);
            actions.moveToElement(quiz.get(i)).build().perform();
            Thread.sleep(3000);


            if (quiz.get(i).findElements(By.xpath(".//a[@class= 'post__quiz-open-btn rocket-green-bg']")).size() > 0)
            {
                WebElement startButton= quiz.get(i).findElement(By.xpath(".//a[@class= 'post__quiz-open-btn rocket-green-bg']"));

                postid= startButton.getAttribute("href");
                System.out.println(postid);
                postid= postid.substring(postid.lastIndexOf("-i-")+3, postid.length());
                System.out.println(postid);

                hashMap= (HashMap<Integer, Integer>) restapi.getpostbyId(postid);

                System.out.println(hashMap);


                actions.keyDown(Keys.SHIFT).click(startButton).keyUp(Keys.SHIFT).build().perform();

                Thread.sleep(5000);


                childWindow= driver.getWindowHandles();

                childWindow.remove(parentWindow);

                Iterator<String> iterator= childWindow.iterator();
                while (iterator.hasNext())
                {
                    driver.switchTo().window(iterator.next());
                    Thread.sleep(3000);

                    for (Map.Entry entry : hashMap.entrySet())
                    {
//                        javascriptExecutor.executeScript("arguments[0].scrollIntoView(true);", driver.findElements(By.xpath(".//ul[@class= 'post__question-options']")).get((Integer) entry.getKey()));
                        Thread.sleep(2000);

                        try {
                            choice= driver.findElements(By.xpath(".//ul[@class= 'post__question-options']")).get((Integer) entry.getKey()).findElements(By.xpath(".//li[@class= 'post__question-option']")).get((Integer) entry.getValue());
                            choice.click();
                        }
                        catch (Exception e)
                        {
                            System.out.println(e);
                        }
//                        javascriptExecutor.executeScript("arguments[0].click();", choice);
                        Thread.sleep(2000);
                    }

                    //Script to share score on web

                    driver.findElement(By.xpath("//div[@class= 'js-share-score-in-comments post__result__share rocket-green-bg safed upper-case']")).click();
                    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@class= 'rocket-green-bg safed js-share-score-comments']")));
                    driver.findElement(By.xpath("//a[@class= 'rocket-green-bg safed js-share-score-comments']")).click();
                    Thread.sleep(8000);

                    driver.close();
                }
                driver.switchTo().window(parentWindow);
                Thread.sleep(2000);
            }
        }




//        WebElement button;
//        for (int i= 0; i< group.getTestCount(); i++)
//        {
//            javascriptExecutor.executeScript("arguments[0].scrollIntoView(true);", group.getStartQuizButtonIndex(i));
//            Thread.sleep(2000);
//            actions.moveToElement(group.getStartQuizButtonIndex(i)).build().perform();
//            Thread.sleep(2000);
//            actions.keyDown(Keys.SHIFT).click(group.getStartQuizButtonIndex(i)).keyUp(Keys.SHIFT).build().perform();
//            //            actions.keyDown(Keys.COMMAND).click(group.getStartQuizButtonIndex(i)).keyUp(Keys.COMMAND).build().perform();
////            javascriptExecutor.executeScript("arguments[0].click();", group.getStartQuizButtonIndex(i));
////            group.clickOnStartQuiz(i);
//            Thread.sleep(3000);
//
//            Set<String> childWindow= driver.getWindowHandles();
//            System.out.println(childWindow);
//            childWindow.remove(parentWindow);
//            System.out.println(childWindow);
//
//            Iterator<String> iterator= childWindow.iterator();
//            while (iterator.hasNext())
//            {
//                driver.switchTo().window(iterator.next());
//                Thread.sleep(3000);
//                driver.close();
//                driver.switchTo().window(parentWindow);
//            }
//        }
    }

    @Test

    public void TC11_GetCorrectOptions()
    {
        String postid= "4add3a80-8644-11e7-823b-a8bfe884c2dd";
        hashMap= (HashMap<Integer, Integer>) restapi.getpostbyId(postid);
        System.out.println(hashMap);
    }

    @Test

    public void TC15_BookmarkQuestions()
    {
        restapi.bookmarkQuestions();
    }

}
