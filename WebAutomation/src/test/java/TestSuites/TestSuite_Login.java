package TestSuites;

import APITest.RESTAPI;
import PageObject.HomePage;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

/**
 * Created by bipin on 22/05/17.
 */
public class TestSuite_Login extends TestBaseSetup
{
    RESTAPI api= new RESTAPI();
    HomePage homePage;
    String cookie;
    WebDriver driver;

    @BeforeMethod
    public void setUp()
    {
        driver=getDriver();
        System.out.println("driver: "+driver);

    }

    @Test

    public void TC01_Login() throws InterruptedException
    {
        homePage= new HomePage(driver);
        System.out.println(homePage);
        wait.until(ExpectedConditions.elementToBeClickable(homePage.getLoginWebElement()));
        homePage.clickLogin();
        homePage.clickOnLogin();
        homePage.emailField().sendKeys("rocking.chap08@gmail.com");
        homePage.clickOnPassword();
        homePage.passwordField().sendKeys("123456");
        homePage.clickOnLoginButton();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@class= 'checkbox checkbox--large checkbox--no-limit']")));
        driver.findElements(By.xpath("//a[@class= 'checkbox checkbox--large checkbox--no-limit']")).get(0).click();
        System.out.println("Logged In successfully!");
        Thread.sleep(5000);
        System.out.println(driver.manage().getCookies());
        cookie= driver.manage().getCookieNamed("connect.sid").toString() + driver.manage().getCookieNamed("__cfduid").toString();
        System.out.println(cookie);
    }


    /*
    @Test

    public void TC01_AttemptQuiz() throws InterruptedException
    {
        Iterator<String> iterator;
        ArrayList<String> arrayList= new ArrayList<String>();
        String postId, optionText, newWindow;
        WebElement postTest;
        Thread.sleep(3000);
        driver.get(url);
        Thread.sleep(3000);

        for (int i= 0; i< 10; i++)
        {
            javascriptExecutor.executeScript("window.scrollTo(0,document.body.scrollHeight)");
            Thread.sleep(3000);
        }

        List<WebElement> postTypeTest= (List<WebElement>) driver.findElements(By.xpath("//article[@class= 'post box-shadow ajax-image-upload-container post--test']"));
        System.out.println(postTypeTest.size());

        for (int i= 0; i< postTypeTest.size(); i++)
        {
            javascriptExecutor.executeScript("arguments[0].scrollIntoView(true)",postTypeTest.get(i));
            Thread.sleep(5000);
            postTest= postTypeTest.get(i).findElement(By.xpath(".//a[@class= 'post__link']"));
            postId= postTypeTest.get(i).findElement(By.xpath(".//a[@class= 'post__link']")).getAttribute("href");
//            System.out.println();
            postId= postId.substring(postId.indexOf("-i-")+3);
            System.out.println(postId);
//            arrayList= api.postsGetPostById(postId, "__cfduid=d2031e9df97d8d3d1982c1a1acebe56411496732307 ; connect.sid=s%3AMBgZvgd6cALv_aAao7X72qR-flbVO_8t.yf%2FqdDz9gE3aX2w1%2FqRYGPQz5GNCJnVv5kksm%2BvNhrA;");
//            System.out.println(arrayList);
//
//            int questionCount= api.getQuestionsCount();
//            System.out.println("Questions Count: "+questionCount);


            actions.keyDown(Keys.SHIFT).click(postTest).keyUp(Keys.SHIFT).build().perform();
            set= driver.getWindowHandles();
            set.remove(parentWindow);

            iterator= set.iterator();



            while (iterator.hasNext())
            {
                newWindow= iterator.next().toString();

                if (!newWindow.contains(parentWindow))
                {
                    driver.switchTo().window(newWindow);
//                List<WebElement> mockTest= driver.findElements(By.xpath("//div[@class= 'mockTestMainContainer']//div[@class= 'mockTestList']//div[contains(@class, 'mockTest')]"));
//                System.out.println("Number of Mock Tests are: "+mockTest.size());
//
//                for (int l= 0; l< mockTest.size(); l++)
//                {
//                    System.out.println("\t\t"+(l+1)+". "+mockTest.get(l).findElement(By.xpath(".//div[@class= 'desc']//h2")).getText()+"\t"+mockTest.get(l).findElement(By.xpath(".//div[@class= 'desc']//p")).getText()+"\t"+mockTest.get(l).findElement(By.xpath(".//div[@class= 'linkContainer']//a[contains(@class, 'btn testLink')]")).getText());
//                }
//                Thread.sleep(5000);
//                System.out.println("\t"+driver.getCurrentUrl());

                    Thread.sleep(5000);
//                    System.out.println("Questions on web page are: "+driver.findElements(By.xpath("//div[@class= 'post__content']//div[@class= 'post__questions']//div[@class= 'post__question']")).size());
//                    for (int l= 0; l< questionCount; l++)
//                    {
////                        javascriptExecutor.executeScript("arguments[0].scrollIntoView(true)", driver.findElements(By.xpath(".//div[@class= 'post__questions']//div[@class= 'post__question']")).get(l));
////                    driver.findElement(By.name(arrayList.get(l))).click();
//                        optionText= arrayList.get(l).trim();
//                        try{
//                            driver.findElement(By.partialLinkText(optionText)).click();
//                        }
//                        catch (Exception e)
//                        {
//                            System.out.println(optionText);
//                        }
//
//                        Thread.sleep(2000);
//                    }

                    javascriptExecutor.executeScript("window.scrollTo(0,document.body.scrollHeight)");
                    Thread.sleep(3000);
                    javascriptExecutor.executeScript("window.scrollTo(0,document.body.scrollHeight)");

                    if(driver.findElements(By.xpath("//a[@class= 'post__submit-test-btn rocket-green-bg js-submit-quiz']")).size() > 0)
                    {
                        javascriptExecutor.executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@class= 'post__submit-test-btn rocket-green-bg js-submit-quiz']")));
                        Thread.sleep(5000);
                        javascriptExecutor.executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@class= 'discard-post-card__discard-btn rocket-bg js-submit-quiz-data']")));
                    }

//                    if (driver.findElements(By.xpath("//div[@class= 'discard-post-actions']")).size() > 0)
//                    {
//
//                    }

                    Thread.sleep(2000);
                    driver.close();
                    driver.switchTo().window(parentWindow);
                    Thread.sleep(5000);
                }
            }
        }
        driver.switchTo().window(parentWindow);


//            arrayList= postsGetPostById(postId, cookie);
//            System.out.println(arrayList);
//            postTypeTest.get(i).findElement(By.xpath(".//a[@class= 'post__link']")).click();
            Thread.sleep(5000);

    }


    @Test

    public void TC_FeaturedQuizList() throws InterruptedException
    {
        String postId;
        String optionText, newWindow;
        Iterator<String> iterator;
        ArrayList<String> arrayList= new ArrayList<String>();
        WebElement showMore= driver.findElement(By.xpath("//a[@class= 'show-more-action u-no-padding-bottom js-show-more grey no-mg-bottom']"));
//        for (int i= 0; i< 2; i++)
//        {
//            javascriptExecutor.executeScript("arguments[0].scrollIntoView(true)", showMore);
//            Thread.sleep(2000);
//            javascriptExecutor.executeScript("arguments[0].click();", showMore);
//        }

        List<WebElement> activeQuiz= driver.findElements(By.xpath("//ul[@class= 'post-info grey post-info--quiz']"));

        for (int i= 0; i< activeQuiz.size(); i++)
        {
            javascriptExecutor.executeScript("arguments[0].scrollIntoView(true)", activeQuiz.get(i));
            Thread.sleep(5000);
//            actions.keyDown(Keys.SHIFT).click(activeQuiz.get(i)).keyUp(Keys.SHIFT).build().perform();
            javascriptExecutor.executeScript("arguments[0].click();", activeQuiz.get(i));
            System.out.println((javascriptExecutor.executeScript("return document.readyState").toString().equals("complete")));



            set= driver.getWindowHandles();
            System.out.println("Parent Window: "+parentWindow);
            System.out.println("Set: "+set);
//            set.remove(parentWindow);
//            System.out.println(set);
            iterator= set.iterator();


            while (iterator.hasNext())
            {
                newWindow= iterator.next().toString();

                if (!newWindow.contains(parentWindow))
                {
                    driver.switchTo().window(newWindow);


                    postId= driver.getCurrentUrl();
                    System.out.println(postId);
                    postId= postId.substring(postId.indexOf("-i-")+3);
//                    System.out.println(postId);
//                    arrayList= api.postsGetPostById(postId, "__cfduid=d2031e9df97d8d3d1982c1a1acebe56411496732307 ; connect.sid=s%3AMBgZvgd6cALv_aAao7X72qR-flbVO_8t.yf%2FqdDz9gE3aX2w1%2FqRYGPQz5GNCJnVv5kksm%2BvNhrA;");
//                    System.out.println(arrayList);
//
//                    int questionCount= api.getQuestionsCount();
//                    System.out.println("Questions Count: "+questionCount);


//                List<WebElement> mockTest= driver.findElements(By.xpath("//div[@class= 'mockTestMainContainer']//div[@class= 'mockTestList']//div[contains(@class, 'mockTest')]"));
//                System.out.println("Number of Mock Tests are: "+mockTest.size());
//
//                for (int l= 0; l< mockTest.size(); l++)
//                {
//                    System.out.println("\t\t"+(l+1)+". "+mockTest.get(l).findElement(By.xpath(".//div[@class= 'desc']//h2")).getText()+"\t"+mockTest.get(l).findElement(By.xpath(".//div[@class= 'desc']//p")).getText()+"\t"+mockTest.get(l).findElement(By.xpath(".//div[@class= 'linkContainer']//a[contains(@class, 'btn testLink')]")).getText());
//                }
//                Thread.sleep(5000);
//                System.out.println("\t"+driver.getCurrentUrl());

                    Thread.sleep(5000);
//                    System.out.println("Questions on web page are: "+driver.findElements(By.xpath("//div[@class= 'post__content']//div[@class= 'post__questions']//div[@class= 'post__question']")).size());
//                    for (int l= 0; l< questionCount; l++)
//                    {
////                        javascriptExecutor.executeScript("arguments[0].scrollIntoView(true)", driver.findElements(By.xpath(".//div[@class= 'post__questions']//div[@class= 'post__question']")).get(l));
////                    driver.findElement(By.name(arrayList.get(l))).click();
//                        optionText= arrayList.get(l).trim();
//                        try{
//                            driver.findElement(By.partialLinkText(optionText)).click();
//                        }
//                        catch (Exception e)
//                        {
//                            System.out.println(optionText);
//                        }
//
//                        Thread.sleep(2000);
//                    }

                    javascriptExecutor.executeScript("window.scrollTo(0,document.body.scrollHeight)");
                    Thread.sleep(3000);
                    javascriptExecutor.executeScript("window.scrollTo(0,document.body.scrollHeight)");

                    if(driver.findElements(By.xpath("//a[@class= 'post__submit-test-btn rocket-green-bg js-submit-quiz']")).size() > 0)
                    {
                        javascriptExecutor.executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@class= 'post__submit-test-btn rocket-green-bg js-submit-quiz']")));
                        Thread.sleep(5000);
                        javascriptExecutor.executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@class= 'discard-post-card__discard-btn rocket-bg js-submit-quiz-data']")));
                    }

//                    if (driver.findElements(By.xpath("//div[@class= 'discard-post-actions']")).size() > 0)
//                    {
//
//                    }

                    Thread.sleep(2000);
                    driver.close();
                    driver.switchTo().window(parentWindow);
                    Thread.sleep(5000);
                }
            }
        }
        driver.switchTo().window(parentWindow);


//            arrayList= postsGetPostById(postId, cookie);
//            System.out.println(arrayList);
//            postTypeTest.get(i).findElement(By.xpath(".//a[@class= 'post__link']")).click();
        Thread.sleep(5000);
    }


    @Test

    public void TC04_ListQuizAttempt() throws InterruptedException
    {
        String postId;
        String optionText, newWindow;
        Iterator<String> iterator;
        ArrayList<String> arrayList= new ArrayList<String>();
        WebElement activeQuiz;
        int questionCount;

        Thread.sleep(2000);
        javascriptExecutor.executeScript("window.scrollTo(0,document.body.scrollHeight);");
        Thread.sleep(2000);
        javascriptExecutor.executeScript("window.scrollTo(0,document.body.scrollHeight);");

        WebElement showMore= driver.findElement(By.xpath("//a[@class= 'show-more-action u-no-padding-bottom js-show-more grey no-mg-bottom']"));

//        if(driver.findElements(By.xpath("//a[@class= 'show-more-action u-no-padding-bottom js-show-more grey no-mg-bottom']")).size() > 0)
//        if(driver.findElements(By.xpath("//a[@title= 'Show More']")).size() > 0)
//        {
//            javascriptExecutor.executeScript("arguments[0].scrollIntoView(true)", showMore);
//            Thread.sleep(2000);
//            javascriptExecutor.executeScript("arguments[0].click();", showMore);
//            Thread.sleep(2000);
//            javascriptExecutor.executeScript("window.scrollTo(0,document.body.scrollHeight);");
//            Thread.sleep(2000);
//            javascriptExecutor.executeScript("window.scrollTo(0,document.body.scrollHeight);");
//            javascriptExecutor.executeScript("window.scrollTo(0,document.body.scrollHeight);");
//            javascriptExecutor.executeScript("window.scrollTo(0,document.body.scrollHeight);");
//            javascriptExecutor.executeScript("window.scrollTo(0,document.body.scrollHeight);");
//        }

        for (int i= 0; i< 40; i++)
//        if(driver.findElements(By.xpath("//a[@title= 'Show More']")).size() > 0)
        {
            javascriptExecutor.executeScript("window.scrollTo(0,document.body.scrollHeight);");
            javascriptExecutor.executeScript("window.scrollTo(0,document.body.scrollHeight);");
            javascriptExecutor.executeScript("window.scrollTo(0,document.body.scrollHeight);");
            javascriptExecutor.executeScript("window.scrollTo(0,document.body.scrollHeight);");
            javascriptExecutor.executeScript("arguments[0].scrollIntoView(true)", showMore);
            Thread.sleep(2000);
            javascriptExecutor.executeScript("window.scrollTo(0,document.body.scrollHeight);");
            javascriptExecutor.executeScript("window.scrollTo(0,document.body.scrollHeight);");
            javascriptExecutor.executeScript("window.scrollTo(0,document.body.scrollHeight);");
            javascriptExecutor.executeScript("window.scrollTo(0,document.body.scrollHeight);");
            javascriptExecutor.executeScript("arguments[0].click();", showMore);
            Thread.sleep(3000);
        }

        Thread.sleep(5000);


        List<WebElement> quiz= driver.findElements(By.xpath("//a[@class= 'list-item flex-box flex-row']"));


        System.out.println("Quiz Size: "+quiz.size());
        for (int i= 0; i< quiz.size(); i++)
        {
            Thread.sleep(1000);
            javascriptExecutor.executeScript("arguments[0].scrollIntoView(true)", quiz.get(i));

            if (quiz.get(i).findElements(By.xpath(".//ul[@class= 'post-info grey post-info--quiz']")).size() > 0)
            {
                activeQuiz= (quiz.get(i)).findElement(By.xpath(".//div[@class= 'list-item__body flex-box flex-column u-flex-grow']"));
//                activeQuiz= (quiz.get(i)).findElement(By.xpath(".//div[@class= 'list-item__body flex-box flex-column u-flex-grow']//h3[@class= 'post-title']]"));
                System.out.println(activeQuiz.getText());
                actions.keyDown(Keys.SHIFT).click((WebElement) activeQuiz).keyUp(Keys.SHIFT).build().perform();
                Thread.sleep(3000);
                System.out.println(driver.getWindowHandles());
                set= driver.getWindowHandles();
                set.remove(parentWindow);

                iterator= set.iterator();



                while (iterator.hasNext())
                {
                    newWindow= iterator.next().toString();

                    if (!newWindow.contains(parentWindow))
                    {
                        driver.switchTo().window(newWindow);


                        postId= driver.getCurrentUrl();
                        System.out.println(postId);
//                        postId= postId.substring(postId.indexOf("-i-")+3);
//                        System.out.println(postId);
//                        arrayList= api.postsGetPostById(postId, "__cfduid=d2031e9df97d8d3d1982c1a1acebe56411496732307 ; connect.sid=s%3AMBgZvgd6cALv_aAao7X72qR-flbVO_8t.yf%2FqdDz9gE3aX2w1%2FqRYGPQz5GNCJnVv5kksm%2BvNhrA;");
//                        System.out.println(arrayList);
//
//                        questionCount= api.getQuestionsCount();
//                        System.out.println("Questions Count: "+questionCount);
//
//

//                List<WebElement> mockTest= driver.findElements(By.xpath("//div[@class= 'mockTestMainContainer']//div[@class= 'mockTestList']//div[contains(@class, 'mockTest')]"));
//                System.out.println("Number of Mock Tests are: "+mockTest.size());
//
//                for (int l= 0; l< mockTest.size(); l++)
//                {
//                    System.out.println("\t\t"+(l+1)+". "+mockTest.get(l).findElement(By.xpath(".//div[@class= 'desc']//h2")).getText()+"\t"+mockTest.get(l).findElement(By.xpath(".//div[@class= 'desc']//p")).getText()+"\t"+mockTest.get(l).findElement(By.xpath(".//div[@class= 'linkContainer']//a[contains(@class, 'btn testLink')]")).getText());
//                }
//                Thread.sleep(5000);
//                System.out.println("\t"+driver.getCurrentUrl());

                        Thread.sleep(1000);
//                    System.out.println("Questions on web page are: "+driver.findElements(By.xpath("//div[@class= 'post__content']//div[@class= 'post__questions']//div[@class= 'post__question']")).size());
//                        for (int l= 0; l< questionCount; l++)
//                        {
////                        javascriptExecutor.executeScript("arguments[0].scrollIntoView(true)", driver.findElements(By.xpath(".//div[@class= 'post__questions']//div[@class= 'post__question']")).get(l));
////                    driver.findElement(By.name(arrayList.get(l))).click();
//                            optionText= arrayList.get(l).trim();
//                            try{
//                                driver.findElement(By.partialLinkText(optionText)).click();
//                            }
//                            catch (Exception e)
//                            {
//                                System.out.println(optionText);
//                            }
//
//                            Thread.sleep(2000);
//                        }

                        javascriptExecutor.executeScript("window.scrollTo(0,document.body.scrollHeight)");
                        Thread.sleep(2000);
                        javascriptExecutor.executeScript("window.scrollTo(0,document.body.scrollHeight)");

                        if(driver.findElements(By.xpath("//a[@class= 'post__submit-test-btn rocket-green-bg js-submit-quiz']")).size() > 0)
                        {
                            javascriptExecutor.executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@class= 'post__submit-test-btn rocket-green-bg js-submit-quiz']")));
                            Thread.sleep(5000);
                            javascriptExecutor.executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@class= 'discard-post-card__discard-btn rocket-bg js-submit-quiz-data']")));
                        }

//                    if (driver.findElements(By.xpath("//div[@class= 'discard-post-actions']")).size() > 0)
//                    {
//
//                    }

                        Thread.sleep(2000);
                        driver.close();
                        driver.switchTo().window(parentWindow);
                        Thread.sleep(5000);
                    }
                    driver.switchTo().window(parentWindow);
                }
            }
        }
    }
    */

}
