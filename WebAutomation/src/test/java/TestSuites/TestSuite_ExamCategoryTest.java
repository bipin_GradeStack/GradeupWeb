package TestSuites;

import APITest.HttpURLConnectionTest;
import APITest.RESTAPI;
import PageObject.ExamCategory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

/**
 * Created by bipin on 11/05/17.
 */
public class TestSuite_ExamCategoryTest extends TestBaseSetup {
    ArrayList<String> examCategory, boxes;
    String urlText;
    HttpURLConnectionTest httpURLConnectionTest= new HttpURLConnectionTest();
    Set<String> childWindow;

    @BeforeMethod

    public void beforeMethod()
    {
        driver=getDriver();
        System.out.println("driver: "+driver);
    }

    @Test

    public void TC01_GetAllSubscribedExamCategories() throws InterruptedException, IOException {
        String urlText;
        examCategory= new ArrayList<String>();
        boxes= new ArrayList<String>();
        Thread.sleep(5000);
        List<WebElement> examCategories= driver.findElements(By.xpath("//ul[@class= 'list-no-style js-explore-list']//li"));
        for (int i= 0; i< examCategories.size(); i++)
        {
            examCategory.add(examCategories.get(i).findElement(By.tagName("a")).getAttribute("href"));
        }

        System.out.println(examCategory);

        /*

        for (int i= 0; i< examCategory.size(); i++)
        {
            driver.get(examCategory.get(i));
            Thread.sleep(2000);

            // Verify if links in Exam Category description returns 200 response or not

            WebElement descriptionBox= driver.findElement(By.xpath("//div[@class= 'box-shadow safed-bg']"));
            List<WebElement> links= descriptionBox.findElements(By.tagName("a"));
            for (int j= 0; j< links.size(); j++)
            {
                if (links.get(j).getAttribute("href").length() > 0)
                {
                    urlText= links.get(j).getAttribute("href");
                    boxes.add(urlText);
                    System.out.println(httpURLConnectionTest.sendGetRequest(urlText));
                }
            }
        }

        for (int k= 0; k< boxes.size(); k++)
        {
            driver.get(boxes.get(k));
            Thread.sleep(5000);
        }
*/
    }


    @Test

    public void TC02_GetExamCategoryDescriptionLink() throws InterruptedException, IOException
    {
        WebElement descriptionBox;
//        driver.get("https://gradeup.co/banking-insurance");
        parentWindow= driver.getWindowHandle();

        for (int j= 0; j< examCategory.size(); j++)
        {
            driver.get(examCategory.get(j));
            Thread.sleep(4000);
            descriptionBox = driver.findElement(By.xpath(".//section[@class= 'description-box']"));
            List<WebElement> links = descriptionBox.findElements(By.tagName("a"));
            for (int i = 0; i < links.size(); i++) {
                //Get all href links from description box!
                if (links.get(i).getAttribute("href").length() > 0) {
                    urlText = links.get(i).getAttribute("href");
                    System.out.println(httpURLConnectionTest.sendGetRequest(urlText));
                    actions.keyDown(Keys.SHIFT).click(links.get(i)).keyUp(Keys.SHIFT).build().perform();
                    childWindow = driver.getWindowHandles();
                    childWindow.remove(parentWindow);
                    Iterator<String> iterator = childWindow.iterator();
                    while (iterator.hasNext()) {
                        driver.switchTo().window(iterator.next());
                        driver.manage().window().maximize();

                        //Check to get page loaded completely
                        ExpectedCondition<Boolean> pageLoadCondition = new
                                ExpectedCondition<Boolean>() {
                                    public Boolean apply(WebDriver driver) {
                                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
                                    }
                                };
                        WebDriverWait wait = new WebDriverWait(driver, 30);
                        wait.until(pageLoadCondition);

                        Thread.sleep(3000);
                        driver.close();
                    }
                    driver.switchTo().window(parentWindow);
                }
            }
        }
    }


    @Test

    public void TC03_OpenLinks() throws InterruptedException
    {

    }
}
