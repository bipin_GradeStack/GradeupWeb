package TestSuites;

import APITest.RESTAPI;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.internal.Executable;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;

/**
 * Created by bipin on 11/05/17.
 */
public class TestSuite_TestSeries extends TestBaseSetup
{
    String url= "https://gradeup.co/runMockTest/23GMPjgYf7+QPY0/td+rbwYoC9vtvP8dVVwInM+gvbVOchofacClGN60NJxkEcl/luYDaONpfOaxv58JAhNZz8xEoXl3xzTuObQ/zCuoPAPYdH4GI42KepKZItCQAlDqjpK9a76WASp9n4JAh3As8V5GIPjFiyhQj5sXB9Jtm6Q=";

    WebDriver driver;
    RESTAPI restapi;

    @BeforeMethod
    public void setUp()
    {
        driver=getDriver();
        System.out.println("driver: "+driver);
        restapi= new RESTAPI();
    }

    /*
    public void openAllPackages() throws InterruptedException
    {
        WebElement element;
        Thread.sleep(5000);
        List<WebElement> viewAll= driver.findElements(By.xpath("//a[@class= 'rocket-green text-uppercase js-package-view-all']"));
        for (int i= 0; i< viewAll.size(); i++)
        {
            javascriptExecutor.executeScript("arguments[0].scrollIntoView(true)", viewAll.get(i));
            wait.until(ExpectedConditions.elementToBeClickable(viewAll.get(i)));
            Thread.sleep(2000);
            element= viewAll.get(i);
            javascriptExecutor.executeScript("arguments[0].click()", element);
            Thread.sleep(2000);
        }

        Iterator<String> iterator;

        List<WebElement> packages= (List<WebElement>)driver.findElements(By.xpath("//div[contains(@class, 'packagedata-container__packages')]"));

        for (int i= 0; i< packages.size(); i++)
        {
            javascriptExecutor.executeScript("arguments[0].scrollIntoView(true)", packages.get(i));
            System.out.println((i+1)+". "+packages.get(i).findElement(By.tagName("h4")).getText()+": "+packages.get(i).findElement(By.xpath(".//span[@class= 'package-price rocket-green']")).getText());
            System.out.println("\t"+packages.get(i).findElements(By.xpath(".//div[@class= 'package-card__footer flex-box']//a")).get(0).getText()+"\t"+packages.get(i).findElements(By.xpath(".//div[@class= 'package-card__footer flex-box']//a")).get(1).getText());
            System.out.println("\t"+packages.get(i).findElements(By.xpath(".//div[@class= 'package-card__footer flex-box']//a")).get(0).getAttribute("href")+"\t"+packages.get(i).findElements(By.xpath(".//div[@class= 'package-card__footer flex-box']//a")).get(1).getAttribute("data-buylink"));

            WebElement childWindow= packages.get(i).findElements(By.xpath(".//div[@class= 'package-card__footer flex-box']//a")).get(0);

            actions.keyDown(Keys.SHIFT).click(childWindow).keyUp(Keys.SHIFT).build().perform();
            set= driver.getWindowHandles();
            set.remove(parentWindow);
            iterator= set.iterator();

            while (iterator.hasNext())
            {
                driver.switchTo().window(iterator.next());

                if (driver.findElements(By.xpath(".//div[@class= 'content-block']")).size() > 0)
                {
                    System.out.println("\t\t\t\t\tGTM Data exists!");
                }
                else
                {
                    System.out.println("\t\t\t\t\t"+driver.getCurrentUrl());
                    System.out.println("\t\t\t\t\tGTM Data doesn't exists!");
                }


                List<WebElement> mockTest= driver.findElements(By.xpath("//div[@class= 'mockTestMainContainer']//div[@class= 'mockTestList']//div[contains(@class, 'mockTest')]"));
                System.out.println("Number of Mock Tests are: "+mockTest.size());

                for (int l= 0; l< mockTest.size(); l++)
                {
                    System.out.println("\t\t"+(l+1)+". "+mockTest.get(l).findElement(By.xpath(".//div[@class= 'desc']//h2")).getText()+"\t"+mockTest.get(l).findElement(By.xpath(".//div[@class= 'desc']//p")).getText()+"\t"+mockTest.get(l).findElement(By.xpath(".//div[@class= 'linkContainer']//a[contains(@class, 'btn testLink')]")).getText());
                    if (mockTest.get(l).findElement(By.xpath("//div[@class= 'linkContainer']//a[@class= 'btn testLink']")).getText().contains("Attempt Now"))
                    {
                        System.out.println("\t\t\t"+mockTest.get(l).findElement(By.xpath(".//div[@class= 'linkContainer']//a[@class= 'btn testLink']")).getAttribute("href"));
                    }
                }
                Thread.sleep(5000);
                System.out.println("\t"+driver.getCurrentUrl());
                driver.close();
                driver.switchTo().window(parentWindow);
                Thread.sleep(5000);
            }
        }
        driver.switchTo().window(parentWindow);
    }


    @Test

    public void TC01_login() throws InterruptedException
    {
//        driver.get("https://gradeup.co/online-test-series");
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//p[@class= 'auth rocket-green-bg safed flex-box flex-row log-reg-btn']")));
        driver.findElement(By.xpath("//a[@class= 'auth__login js-show-login']")).click();
        driver.findElements(By.xpath("//input[@type= 'email']")).get(2).sendKeys("rocking.chap08@gmail.com");
        driver.findElements(By.xpath("//input[@type= 'password']")).get(2).sendKeys("123456");
        driver.findElements(By.xpath("//input[@type= 'submit']")).get(3).click();
        driver.findElements(By.xpath("//a[@class= 'checkbox checkbox--large checkbox--no-limit']")).get(0).click();
        Thread.sleep(3000);
    }

    @Test

    public void TC02_SubscribeAllExamPackages() throws InterruptedException
    {
        driver.get("https://gradeup.co/online-test-series");
        Thread.sleep(12000);
        WebElement examcategory= driver.findElement(By.xpath("//input[@class= 'filter-container__input__box filter-container__input__exam-category form-control js-filter-exam-category']"));
        wait.until(ExpectedConditions.elementToBeClickable(examcategory));
        javascriptExecutor.executeScript("arguments[0].click", examcategory);
        List<WebElement> exams= driver.findElements(By.xpath("//li[@class= 'js-filter-exam-category__option overflow-ellipsis']"));
        System.out.println(exams.size());
        Thread.sleep(12000);

        for (int i= 0; i< exams.size(); i++)
        {
            javascriptExecutor.executeScript("document.getElementById(\"examCategory\").scrollIntoView(true);");
            javascriptExecutor.executeScript("arguments[0].click()", examcategory);
            System.out.println(exams.get(i).getText());
            javascriptExecutor.executeScript("arguments[0].click()", exams.get(i));
            Thread.sleep(3000);
            openAllPackages();
            System.out.println("*************************************************");
            System.out.println(driver.getCurrentUrl());
        }
    }

*/
    @Test

    public void TC03_AttemptTest() throws InterruptedException
    {

        Thread.sleep(3000);
        driver.get(url);

        int questionsCount;
        List<WebElement> questions, questionTab;
        Thread.sleep(3000);
//        driver.findElements(By.xpath("//a[@class= 'package__button package-card__footer__button text-uppercase google-red-bg google-red-border']")).get(1).click();
//        Thread.sleep(3000);
//
//        System.out.println("Package URL is: "+driver.getCurrentUrl());
//        List<WebElement> mockTest= driver.findElements(By.xpath("//div[@class= 'mockTestMainContainer']//div[@class= 'mockTestList']//div[contains(@class, 'mockTest')]"));
//        System.out.println("Number of Mock Tests are: "+mockTest.size());
//
//        for (int i= 0; i< mockTest.size(); i++)
//        {
//            System.out.println((i+1)+". "+mockTest.get(i).findElement(By.xpath(".//div[@class= 'desc']//h2")).getText()+"\t"+mockTest.get(i).findElement(By.xpath(".//div[@class= 'desc']//p")).getText()+"\t"+mockTest.get(i).findElement(By.xpath(".//div[@class= 'linkContainer']//a[contains(@class, 'btn testLink')]")).getText());
//        }

//        String mockTestStatus= driver.findElements(By.xpath("//div[@class= 'mockTest active']//div[@class= 'linkContainer']//a[@class= 'btn testLink resume']")).get(0).getText();


//        driver.findElement(By.xpath("//a[contains(@class= 'btn testLink')]")).click();
//        driver.findElement(By.xpath("//a[contains(@class= 'btn testLink') and contains(text()= 'Attempt Now')]")).click();

//        Start Test Button Link
//        driver.findElements(By.xpath("//div[@class= 'mockTest active']//div[@class= 'linkContainer']//a[@class= 'btn testLink']")).get(0).click();
//        Thread.sleep(5000);
//        javascriptExecutor.executeScript("window.scrollTo(0, document.body.scrollHeight)");
//        javascriptExecutor.executeScript("window.scrollTo(0, document.body.scrollHeight)");
//        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id= 'instPagination']//a")));
//        driver.findElement(By.xpath("//div[@id= 'instPagination']//a")).click();
//        driver.findElement(By.xpath("//input[@id= 'disclaimer']")).click();
//        driver.findElement(By.linkText("I am ready to begin")).click();




        //Resume Quiz Button Link
//        driver.findElements(By.xpath("//div[@class= 'mockTest active']//div[@class= 'linkContainer']//a[@class= 'btn testLink resume']")).get(0).click();
        Thread.sleep(3000);
        javaScriptExecutor.executeScript("arguments[0].click()", driver.findElement(By.xpath("//input[@class= 'btn btn-primary resumeButton']")));

        Thread.sleep(5000);
        List<WebElement> sections= driver.findElements(By.xpath("//a[contains(@class, 'test-link section-link')]"));
        System.out.println("Sections Size is: "+sections.size());
//        for (int i= 0; i< sections.size(); i++)
//        {
//            wait.until(ExpectedConditions.elementToBeClickable(sections.get(i)));
//            System.out.println(sections.get(i).getText());
//            List<WebElement> palette= driver.findElements(By.xpath("//span[contains(@class, 'questions_list questionsUL')]"));
//            int count= palette.size();
//            System.out.println("Count of Questions Palette is: "+count);
//
////            System.out.println("Questions count is: "+palette.get(i).findElements(By.xpath("//a[contains(@class, 'quesLinksa questionlistLinks q')]]")).size());
//
//            for (int j= 0; j< count; j++)
//            {
//                System.out.println(driver.findElements(By.xpath("//a[contains(@class, 'quesLinksa questionlistLinks q status-')]")).get(j).getText());
//                driver.findElement(By.xpath("//input[@id= 'option0']")).click();
//                Thread.sleep(2000);
//                driver.findElement(By.xpath("//input[@class= 'SNBtn _next']")).click();
//                Thread.sleep(2000);
//            }
//        }


        Thread.sleep(5000);
        List<WebElement> sections1= driver.findElements(By.xpath("//a[contains(@class, 'test-link section-link')]"));
        System.out.println("Sections Size is: "+sections1.size());
        sections1.get(0).click();
        Thread.sleep(5000);

        List<WebElement> sections2= driver.findElements(By.xpath("//div[@class= 'quesLinks']//span[@class= 'questions_list questionsUL']"));

        int count1= sections2.size();
        System.out.println(count1);

        String id;
        for (int i= 0; i< count1; i++)
        {
            id= "questionsUL"+i;
            questionsCount= Integer.valueOf(javaScriptExecutor.executeScript("return document.getElementById('" + id + "').children.length").toString());

            System.out.println((i+1)+"Questions count is: "+questionsCount);

            for (int j= 0; j< questionsCount; j++)
            {
                driver.findElement(By.xpath("//input[@id= 'option0']")).click();
                Thread.sleep(2000);
                driver.findElement(By.xpath("//input[@class= 'SNBtn _next']")).click();
                Thread.sleep(2000);
            }




            try {
                wait.until(ExpectedConditions.alertIsPresent());
                driver.switchTo().alert().accept();
                System.out.println("Alert Accepted!");
            }
            catch (Exception e)
            {
                System.out.println("Alert Absent!");
            }
        }
        driver.findElement(By.xpath("//a[@id= 'examSummary']")).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath("//input[@id= 'finish_btn']")).click();
        Thread.sleep(5000);
        driver.switchTo().alert().accept();
        Thread.sleep(5000);
        driver.findElement(By.xpath("//input[@class= 'finalFinishButton']")).click();
        Thread.sleep(5000);


    }

}
