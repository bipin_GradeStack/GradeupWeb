package TestSuites;

import APITest.RESTAPI;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

public class TestBaseSetup {

    static WebDriver driver;
    static WebDriverWait wait;
    static Actions actions;
    static JavascriptExecutor javaScriptExecutor;
    static String driverPath = "/Users/bipin/Downloads/chromedriver";
    static String parentWindow= "";
//    RESTAPI restapi;


    public static WebDriver setDriver(String appURL)
    {
//        browserType= "chrome";
        driver = initChromeDriver(appURL);
//        switch (browserType) {
//            case "chrome":
//                driver = initChromeDriver(appURL);
//                break;
//            case 'f':
//                driver = initFirefoxDriver(appURL);
//                break;
//            default:
//                System.out.println("browser : " +browserType+" is invalid, Launching Firefox as browser of choice..");
//                driver = initFirefoxDriver(appURL);
//        }
        return driver;
    }

    public static WebDriver initChromeDriver(String appURL)
    {
        System.out.println("Launching google chrome with new profile..");
        System.setProperty("webdriver.chrome.driver", driverPath);
        ChromeOptions options= new ChromeOptions();
        options.addArguments("-incognito");
        options.addArguments("start-fullscreen");

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        LoggingPreferences loggingprefs = new LoggingPreferences();
        loggingprefs.enable(LogType.BROWSER, Level.SEVERE);
        capabilities.setCapability(CapabilityType.LOGGING_PREFS, loggingprefs);
        WebDriver driver = new ChromeDriver(capabilities);
        javaScriptExecutor = (JavascriptExecutor)driver;
        actions= new Actions(driver);

        driver.get("chrome://extensions-frame");
        WebElement checkbox = driver.findElement(By.xpath("//label[@class='incognito-control']/input[@type='checkbox']"));
        if (!checkbox.isSelected()) {
            checkbox.click();
        }

        driver.manage().timeouts().implicitlyWait(15000, TimeUnit.MILLISECONDS);
//        driver.get(baseURL);

//        currentWindowHandle = this.driver.getWindowHandle();
//        driver.switchTo().window(currentWindowHandle);
//
//        wait= new WebDriverWait(driver, 30);
//        set= new HashSet<String>();
//        actions= new Actions(driver);
//        parentWindow= driver.getWindowHandle();
//        System.out.println("Before Test executed!");

        wait= new WebDriverWait(driver, 30);
        driver.navigate().to(appURL);
        return driver;
    }

    public static WebDriver initFirefoxDriver(String appURL) {
        System.out.println("Launching Firefox browser..");
        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.navigate().to(appURL);
        return driver;
    }

    @Parameters({ "appURL" })
    @BeforeSuite
    public void initializeTestBaseSetup(String appURL) {
        System.out.println("Inside Initializing!!!");
        try {
            driver= setDriver(appURL);
        } catch (Exception e) {
            System.out.println("Error....." + e.getStackTrace());
        }
    }

    @AfterSuite
    public void tearDown() {
        driver.quit();
    }

    public WebDriver getDriver()
    {
        return driver;
    }
}
