package TestSuites;

import PageObject.HomePage;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import java.io.*;
import java.lang.ref.PhantomReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

/**
 * Created by bipin on 11/05/17.
 */
public class TestSuite_HomePageTest{
    WebDriver driver;
    HomePage homePage;
    WebDriverWait wait;
    JavascriptExecutor jse;
    String baseURL= "https://gradeup.co/online-test-series";
    File file;
    InputStream XlsxFileToRead = null;
    XSSFWorkbook workbook = null;
    DataFormatter dataFormatter= new DataFormatter();
    FileOutputStream fileOutputStream;
    ArrayList<String> arrayListURL, arrayListImage;

    ConcurrentHashMap<String, String> hashMap= new ConcurrentHashMap<String, String>();




//    WebDriver driver;
//    String baseURL= "https://gradeup.co";
//    JavascriptExecutor jse;
    String currentWindowHandle;
//    WebDriverWait wait;
    String parentWindow;
    Set<String> set;
    Actions actions;


    public void verifyURL(String link) throws Exception
    {
        URL url = new URL(link);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        connection.setRequestProperty("accept-encoding", "gzip, deflate, sdch, br");
        connection.setRequestProperty("accept-language", "en-US,en;q=0.8,hi;q=0.6");
        connection.setRequestProperty("user-agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36");
        System.out.println(url);
        System.out.println("Response code : " + connection.getResponseCode());
        Assert.assertEquals(connection.getResponseCode(), 200);
    }


    @BeforeTest

    public void beforeTest()
    {
        arrayListURL= new ArrayList<String>();
        arrayListImage= new ArrayList<String>();
//        homePage= new HomePage(driver);

        System.setProperty("webdriver.chrome.driver", "/Users/bipin/Downloads/chromedriver");

        jse = (JavascriptExecutor)driver;

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        driver = new ChromeDriver(capabilities);

        ChromeOptions options= new ChromeOptions();
        options.addArguments("-incognito");
        options.addArguments("start-fullscreen");

        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        LoggingPreferences loggingprefs = new LoggingPreferences();
        loggingprefs.enable(LogType.BROWSER, Level.SEVERE);
        capabilities.setCapability(CapabilityType.LOGGING_PREFS, loggingprefs);

        driver.get("chrome://extensions-frame");
        WebElement checkbox = driver.findElement(By.xpath("//label[@class='incognito-control']/input[@type='checkbox']"));
        if (!checkbox.isSelected()) {
            checkbox.click();
        }

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.get(baseURL);

        currentWindowHandle = this.driver.getWindowHandle();
        driver.switchTo().window(currentWindowHandle);

        wait= new WebDriverWait(driver, 30);
        set= new HashSet<String>();
        actions= new Actions(driver);
        parentWindow= driver.getWindowHandle();
        System.out.println("Before Test executed!");
    }

    @AfterTest
    public void afterTest()
    {
        for (Map.Entry entry: hashMap.entrySet())
        {
            System.out.println(entry.getKey()+": "+entry.getValue());
        }

        LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER);
        for (LogEntry entry : logEntries) {
            System.out.println(new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage());
        }
        driver.quit();
    }

//    @BeforeMethod

    public void beforeMethod()
    {
        file= new File("/home/bipin/IdeaProjects/GradeupWebLive/src/test/java/data/LiveServer.xlsx");
        try
        {
            XlsxFileToRead = new FileInputStream(file);
            workbook = new XSSFWorkbook(XlsxFileToRead);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

//    @AfterMethod

    public void afterMethod()
    {
        try {
            XlsxFileToRead.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Test(priority = 1)

    public void TC_Login() throws InterruptedException
    {
        Thread.sleep(5000);
        driver.findElement(By.xpath("//a[@class= 'auth__login js-show-login']")).click();
        driver.findElements(By.xpath("//input[@name= 'email']")).get(2).sendKeys("rocking.chap08@gmail.com");
        driver.findElements(By.xpath("//input[@name= 'password']")).get(2).sendKeys("123456");
        driver.findElements(By.xpath("//input[@class= 'btn auth__button js-auth-login']")).get(1).click();
    }


//    @Test

    public void verifyText() throws InterruptedException
    {
        XSSFSheet sheet = workbook.getSheet("Sheet1");
        int rowCount= sheet.getLastRowNum()-sheet.getFirstRowNum();
        Row row= sheet.getRow(1);
        homePage= new HomePage(driver);
        String text= homePage.getStudyText();
        Assert.assertEquals(homePage.getStudyText(), "Study");
        Assert.assertEquals(homePage.loginText(), "Login");
        Assert.assertEquals(homePage.registerText(), "Register");
        Assert.assertEquals(homePage.searchText(), "Search for posts, exams & users...");
        Assert.assertEquals(homePage.registerNowText(), "REGISTER NOW");
        Assert.assertEquals(homePage.getloginAgainText().get(0).getText(), "REGISTER NOW");
        Assert.assertEquals(homePage.getloginAgainText().get(1).getText(), "Already have an account?  LOGIN");
        Assert.assertEquals(homePage.getloginAgainText().get(2).getText(), "LOGIN");
        System.out.println(homePage.getStudyExams().size());
        homePage.clickStudy();
        for (int i= 0; i< homePage.getStudyExams().size(); i++)
        {
            Assert.assertEquals(homePage.getStudyExams().get(i).getText(), sheet.getRow(i+1).getCell(0).getStringCellValue());
        }
        homePage.clickStudy();
        Assert.assertEquals(homePage.getImageSource(), sheet.getRow(15).getCell(1).getStringCellValue());
        Assert.assertEquals(homePage.getGooglePlaySource(), sheet.getRow(16).getCell(1).getStringCellValue());
        Assert.assertEquals(homePage.getSMStext(), sheet.getRow(17).getCell(1).getStringCellValue());
        Assert.assertEquals(homePage.getGooglePlayText(), sheet.getRow(18).getCell(1).getStringCellValue());
        Assert.assertEquals(homePage.getUsersCountText(), sheet.getRow(19).getCell(1).getStringCellValue());
        Assert.assertEquals(homePage.getDownloadText(), sheet.getRow(20).getCell(1).getStringCellValue());
        Assert.assertEquals(homePage.getLinkText(), sheet.getRow(21).getCell(1).getStringCellValue());
        Assert.assertEquals(homePage.getMobilePlaceHolderText(), sheet.getRow(22).getCell(1).getStringCellValue());
        Assert.assertEquals(homePage.getAboutUSText(), sheet.getRow(23).getCell(0).getStringCellValue());
        Assert.assertEquals(homePage.getAboutUSLink(), sheet.getRow(23).getCell(1).getStringCellValue());
        Assert.assertEquals(homePage.getFAQText(), sheet.getRow(24).getCell(0).getStringCellValue());
        Assert.assertEquals(homePage.getFAQLink(), sheet.getRow(24).getCell(1).getStringCellValue());
        Assert.assertEquals(homePage.getTermsText(), sheet.getRow(25).getCell(0).getStringCellValue());
        Assert.assertEquals(homePage.getTermsLink(), sheet.getRow(25).getCell(1).getStringCellValue());
        Assert.assertEquals(homePage.getCompanyName(), sheet.getRow(26).getCell(1).getStringCellValue());
        Assert.assertEquals(homePage.getFacebookTitle(), sheet.getRow(27).getCell(0).getStringCellValue());
        Assert.assertEquals(homePage.getFacebookLink(), sheet.getRow(27).getCell(1).getStringCellValue());
        Assert.assertEquals(homePage.getTwitterTitle(), sheet.getRow(28).getCell(0).getStringCellValue());
        Assert.assertEquals(homePage.getTwitterLink(), sheet.getRow(28).getCell(1).getStringCellValue());
        Thread.sleep(5000);
    }


    public void getURL()
    {
        String link;
        List<WebElement> url= driver.findElements(By.tagName("a"));
        for (int i= 0; i< url.size(); i++)
        {
            link= url.get(i).getAttribute("href");

            if (link!= null && link.contains("gradeup.co") && (!hashMap.containsKey(link)))
            {
//                if(!hashMap.get(link).equals("done"))
//                {
                hashMap.put(link, "pending");
//                }
            }
        }


        for (Map.Entry<String, String> entry: hashMap.entrySet())
        {
            System.out.println(entry.getKey()+": "+entry.getValue());
        }



//        List<WebElement> image= driver.findElements(By.tagName("img"));
//        for (int i= 0; i< image.size(); i++)
//        {
//            link= image.get(i).getAttribute("src");
//            if(link!= null && link.contains("http") && (!arrayListImage.contains(link)))
//            {
//                arrayListImage.add(link);
//            }
//        }
//        System.out.println(arrayListImage);

    }

//    @Test(priority = 2)
//
//    public void TC02_VerifyAllLinks() throws Exception
//    {
//        for (int i= 0; i< arrayListURL.size(); i++)
//        {
//            verifyURL(arrayListURL.get(i));
//        }
//
//        for (int i= 0; i< arrayListImage.size(); i++)
//        {
//            verifyURL(arrayListImage.get(i));
//        }
//    }

    @Test(priority = 3)

    public void TC03_OpenAllLinks() throws InterruptedException
    {
        Thread.sleep(2000);
        getURL();

//        Iterator<Map.Entry<String, String>> iterator= hashMap.entrySet().iterator();
//
//        while(iterator.hasNext() && )
//        {
//
//        }

        /*
        for (Map.Entry entry: hashMap.entrySet())
        {
            if (entry.getValue().equals("pending"))
            {
                driver.get((String) entry.getKey());
                getURL();
                Thread.sleep(3000);
                entry.setValue("done");
            }
        }
        for (Map.Entry entry: hashMap.entrySet())
        {
            System.out.println(entry.getKey()+": "+entry.getValue());
        }
        */
    }
}
