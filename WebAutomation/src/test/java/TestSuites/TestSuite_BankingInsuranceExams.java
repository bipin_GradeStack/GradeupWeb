package TestSuites;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

/**
 * Created by bipin on 11/05/17.
 */
public class TestSuite_BankingInsuranceExams {
    File file;
    WebDriver driver;
    WebDriverWait wait;
    String baseURL= "https://gradeup.co/computer-science-engineering/subjects";
    String sheetName= "CSE-Subjects";
    XSSFWorkbook workbook;
    FileInputStream fileInputStream;
    FileOutputStream fileOutputStream;
    XSSFSheet sheet;

    @BeforeTest

    public void setup() throws IOException, InvalidFormatException {
        file= new File("/home/bipin/IdeaProjects/GradeupWebLive/src/test/java/data/hello.xlsx");
        System.setProperty("webdriver.chrome.driver", "/home/bipin/chromedriver");
        ChromeOptions options= new ChromeOptions();
        options.addArguments("-incognito");
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        LoggingPreferences loggingprefs = new LoggingPreferences();
        loggingprefs.enable(LogType.BROWSER, Level.ALL);
        capabilities.setCapability(CapabilityType.LOGGING_PREFS, loggingprefs);
        driver = new ChromeDriver(capabilities);

        driver.get("chrome://extensions-frame");
        WebElement checkbox = driver.findElement(By.xpath("//label[@class='incognito-control']/input[@type='checkbox']"));
        if (!checkbox.isSelected()) {
            checkbox.click();
        }

        wait= new WebDriverWait(driver, 100);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.get(baseURL);
        driver.manage().window().maximize();


        FileInputStream fileInputStream= new FileInputStream(file);
        workbook = new XSSFWorkbook(fileInputStream);
        System.out.println("Before Test executed!");

    }

    @AfterTest
    public void afterTest() throws IOException
    {
        LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER);
        for (LogEntry entry : logEntries) {
            System.out.println(new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage());
            driver.quit();
        }
    }

    @Test

    public void test() throws InterruptedException {
        driver.findElement(By.xpath("//div[@class= 'main-header__user main-header__sub-menu main-header__explore-link']")).click();
        Thread.sleep(5000);
        List<WebElement> elements= driver.findElements(By.xpath("//a[@class, ' s-15 main-header__explore-panel-item']"));

        for (int i= 0; i< elements.size(); i++)
        {
            System.out.println(elements.get(i).getText());
            System.out.println(elements.get(i).getAttribute("href"));
        }

//        Cell cell;
//        List<WebElement> element= driver.findElements(By.xpath("//a[@class= 'box__item']"));
//        int size= element.size();
//        XSSFSheet sheet = workbook.createSheet(sheetName);
//
//
//            for (int i= 0; i< size; i++)
//            {
//                System.out.println(element.get(i).findElement(By.tagName("h2")).getText());
////                Cell cell= sheet.createRow(i).createCell(i);
////                cell.setCellValue(element.get(i).findElement(By.tagName("h2")).getText());
//                cell= sheet.createRow(i+1).createCell(0);
//                cell.setCellValue(String.valueOf(element.get(i).findElement(By.tagName("h2")).getText()));
//                System.out.println(element.get(i).findElement(By.tagName("img")).getAttribute("src"));
//                cell= sheet.getRow(i+1).createCell(1);
//                cell.setCellValue(String.valueOf(element.get(i).findElement(By.tagName("img")).getAttribute("src")));
//                System.out.println(element.get(i).getAttribute("href"));
//                cell= sheet.getRow(i+1).createCell(2);
//                cell.setCellValue(String.valueOf(element.get(i).getAttribute("href")));
//                System.out.println();
//            }
//
//        fileOutputStream = new FileOutputStream(file);
//        workbook.write(fileOutputStream);
//        fileOutputStream.flush();
//        fileOutputStream.close();
    }

}
